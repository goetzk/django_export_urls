import pytest
import mock

from django_export_urls.management.commands import configure_webserver

def test_override_simplify_regex():
  assert '&^%GHBVT' is configure_webserver.override_simplify_regex('&^%GHBVT')
  assert r'x' is configure_webserver.override_simplify_regex(r'x')
  assert u'foo' is configure_webserver.override_simplify_regex(u'foo')

def test_format_output_against_nginx_lower():
  configure_webserver.EXPORT_URLS_FORMAT = 'nginx'
  assert 'location ~ teststr { return 404; }' == configure_webserver.format_output('teststr')

def test_format_output_against_nginx_upper():
  configure_webserver.EXPORT_URLS_FORMAT = 'NGINX'
  assert 'location ~ teststr { return 404; }' == configure_webserver.format_output('teststr')

def test_format_output_against_apache24_lower():
  configure_webserver.EXPORT_URLS_FORMAT = 'apache24'
  assert 'RewriteRule teststr [L,R=404]' == configure_webserver.format_output('teststr')

def test_format_output_against_apache24_upper():
  configure_webserver.EXPORT_URLS_FORMAT = 'APACHE24'
  assert 'RewriteRule teststr [L,R=404]' == configure_webserver.format_output('teststr')

def test_format_output_against_text_lower():
  configure_webserver.EXPORT_URLS_FORMAT = 'text'
  assert 'teststr' == configure_webserver.format_output('teststr')

def test_format_output_against_text_upper():
  configure_webserver.EXPORT_URLS_FORMAT = 'TEXT'
  assert 'teststr' == configure_webserver.format_output('teststr')

def test_format_output_against_unknown():
  configure_webserver.EXPORT_URLS_FORMAT = 'unknown format value'
  assert 'teststr' == configure_webserver.format_output('teststr')

# @mock.patch('django_export_urls.management.commands.configure_webserver.', autospec=True)
@mock.patch('django_export_urls.management.commands.configure_webserver.write_out_urls', autospec=True)
@mock.patch('django_export_urls.management.commands.configure_webserver.open')
def test_write_out_urls(mock_fileio, mock_write_out_urls):
  mock_fileio.return_value = None
  mock_fileio.open.return_value = mock_fileio
  mock_fileio.write.return_value = mock_fileio
  mock_fileio.close.return_value = mock_fileio
  # Three records should be written.
  result = configure_webserver.write_out_urls([' x', 'one', 'two', 'three'])
  # TODO suitable asserts, for example this sort of thing:
  # assert mock_fileio.open.called
  # assert mock_fileio.close.called
  # assert mock_fileio.write.call_count is 4

@mock.patch('django_export_urls.management.commands.configure_webserver.write_out_urls', autospec=True)
@mock.patch('django_export_urls.management.commands.configure_webserver.super')
def test_management_command_runs(mock_super, mock_write_out_urls):
  mock_super.return_value = mock_super
  mock_super.split.return_value = ['x', 'duplicateurl', 'valid', 'otherurl', 'duplicateurl']
  mock_super.handle.return_value = 'a text string'

  c = configure_webserver.Command()
  c.handle()

  assert mock_super.called
  assert mock_write_out_urls.called

