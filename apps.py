"""Run URL export when Django loads."""

from django.apps import AppConfig

class UrlExporterAppConfig(AppConfig):
    """Set application settings."""
    name='django_export_urls'
    verbose_name='Django URL exporter'

    def ready(self):
        from django.core import management
        from django_export_urls.management.commands import configure_webserver

        management.call_command('configure_webserver')

